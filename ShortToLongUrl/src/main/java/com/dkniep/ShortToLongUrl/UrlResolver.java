package com.dkniep.ShortToLongUrl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class UrlResolver {

    public ResolvedUrl Resolve(Url urlToResolve) throws IOException {

        HttpURLConnection connection = createHttpURLConnection(urlToResolve);
        Integer responseCode = connection.getResponseCode();
        if(IsRedirect(responseCode))
        {
            String location = connection.getHeaderField("Location");
            Url locationAsUrl = new Url(location);
            ResolvedUrl recursiveResolved = Resolve(locationAsUrl);
            return new ResolvedUrl(urlToResolve, recursiveResolved.GetTargetUrl());
        }
        return new ResolvedUrl(urlToResolve, urlToResolve);
    }

    private HttpURLConnection createHttpURLConnection(Url urlToResolve) throws IOException {
        URL url = new URL(urlToResolve.GetUrl());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setInstanceFollowRedirects(false);
        return connection;
    }

    private boolean IsRedirect(Integer responseCode) {
        return responseCode == HttpURLConnection.HTTP_MOVED_PERM ||
                responseCode == HttpURLConnection.HTTP_MOVED_TEMP ||
                responseCode == HttpURLConnection.HTTP_SEE_OTHER;
    }
}
