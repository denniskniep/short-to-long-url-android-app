package com.dkniep.ShortToLongUrl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlExtractor  {

    private final String _UrlPattern = "http(s?)://.*?(\\s|$)";

    public List<Url> ExtractUrls(String text)  {
        Pattern pattern = Pattern.compile(_UrlPattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);

        List<Url> urls = new ArrayList<Url>();
        while (matcher.find()) {

            String url = text.substring(matcher.start(),matcher.end());
            urls.add(new Url(url));
        }
        return urls;
    }
}
