package com.dkniep.ShortToLongUrl;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setLinkToAppWebSite();
    }

    private void setLinkToAppWebSite() {
        TextView link = (TextView) findViewById(R.id.main_appwebsitelink);
        link.setMovementMethod(LinkMovementMethod.getInstance());
        CharSequence text = getResources().getText(R.string.visitwebsite);
        link.setText(Html.fromHtml(text.toString()));
    }
}
