package com.dkniep.ShortToLongUrl;

import java.util.List;

public class ResolvedUrlReplacer {

    public String Replace(String text, List<ResolvedUrl> resolvedUrls)
    {
        String replacedText = text;
        for( ResolvedUrl resolvedUrl: resolvedUrls )
        {
            replacedText = Replace(replacedText, resolvedUrl);
        }
        return replacedText;
    }

    private String Replace(String text, ResolvedUrl resolvedUrl)
    {
        return text.replace(resolvedUrl.GetRootUrl().GetUrl(), resolvedUrl.GetTargetUrl().GetUrl());
    }
}
