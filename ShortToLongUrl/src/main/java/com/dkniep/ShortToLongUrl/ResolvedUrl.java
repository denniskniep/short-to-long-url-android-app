package com.dkniep.ShortToLongUrl;

public class ResolvedUrl {
    private Url _RootUrl;
    private Url _TargetUrl;

    public ResolvedUrl(Url rootUrl, Url targetUrl)
    {
        _RootUrl = rootUrl;
        _TargetUrl = targetUrl;
    }

    public Url GetRootUrl()
    {
        return _RootUrl;
    }

    public Url GetTargetUrl()
    {
        return _TargetUrl;
    }
}
