package com.dkniep.ShortToLongUrl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UrlsInTextResolver {

    public String Resolve(String textToResolve) throws IOException {
        List<Url> extractedUrls = ExtractUrls(textToResolve);
        List<ResolvedUrl> resolvedUrls = resolveUrls(extractedUrls);
        return ReplaceResolvedUrls(textToResolve, resolvedUrls);
    }

    private String ReplaceResolvedUrls(String textToResolve, List<ResolvedUrl> resolvedUrls) {
        ResolvedUrlReplacer replacer = new ResolvedUrlReplacer();
        return replacer.Replace(textToResolve,resolvedUrls);
    }

    private List<ResolvedUrl> resolveUrls(List<Url> extractedUrls) throws IOException {
        List<ResolvedUrl> resolvedUrls = new ArrayList<ResolvedUrl>();
        UrlResolver resolver = new UrlResolver();

        for( Url extractedUrl : extractedUrls )
        {
            resolvedUrls.add(resolver.Resolve(extractedUrl));
        }
        return resolvedUrls;
    }

    private List<Url> ExtractUrls(String textToResolve) {
        UrlExtractor extractor = new UrlExtractor();
        return extractor.ExtractUrls(textToResolve);
    }

}
