package com.dkniep.ShortToLongUrl;

public class Url {

    private String _Url;

    public Url(String url) {
        _Url = url.trim();
    }

    public String GetUrl()
    {
        return _Url;
    }
}
