package com.dkniep.ShortToLongUrl;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;

public class IncomingContentActivity extends Activity {

    public static final String TEXT_PLAIN = "text/plain";
    private boolean _Sharing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incomingcontent);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (isPlainTextIntent(action, type)) {
            handlePlainText(intent);
        }
    }

    private boolean isPlainTextIntent(String action, String type) {
        return Intent.ACTION_SEND.equals(action) && TEXT_PLAIN.equals(type);
    }

    void handlePlainText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            ResolveUrlsInTextAndShareTask resolveAndShareTask = new ResolveUrlsInTextAndShareTask(this);
            resolveAndShareTask.execute(sharedText);
        }
    }

    private void sharePlainText(String textToShare) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, textToShare);
        sendIntent.setType(TEXT_PLAIN);
        Intent chooserIntent = Intent.createChooser(sendIntent, getResources().getText(R.string.send_to));
        _Sharing = true;
        startActivity(chooserIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        finishIfSharing();
    }

    private void finishIfSharing() {
        if(_Sharing)
        {
            _Sharing = false;
            finish();
        }
    }

    @Override
       protected void onStart() {
        super.onStart();
    }

    class ResolveUrlsInTextAndShareTask extends AsyncTask<String, Void, String> {

        private Exception _Exception;
        private IncomingContentActivity _IncomingContentActivity;
        private LinearLayout _ProgressBarLayout;

        public ResolveUrlsInTextAndShareTask(IncomingContentActivity incomingContentActivity){

            _IncomingContentActivity = incomingContentActivity;
            _ProgressBarLayout = (LinearLayout)_IncomingContentActivity.findViewById(R.id.incomingContent_ProgressBarLayout);
        }

        @Override
        protected String doInBackground(String... strings) {
            String inputText = strings[0];
            try {
                    UrlsInTextResolver resolver = new UrlsInTextResolver();
                    return resolver.Resolve(inputText);
            } catch (Exception ex) {
                _Exception = ex;
                return inputText;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            _ProgressBarLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String textToShare) {
            super.onPostExecute(textToShare);

            if(_Exception != null)
            {
                CharSequence message =  _IncomingContentActivity.getResources().getText(R.string.process_resolve_error);
                message = String.format(message.toString(),_Exception.toString(), _Exception.getMessage());
                Toast exceptionToast = Toast.makeText(_IncomingContentActivity, message, Toast.LENGTH_LONG);
                exceptionToast.show();
            }

            _IncomingContentActivity.sharePlainText(textToShare);
            _ProgressBarLayout.setVisibility(View.GONE);
        }
    }
}



