package com.dkniep.ShortToLongUrl;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class ResolvedUrlReplacerTest extends TestCase {

    public ResolvedUrlReplacerTest(){
        super();
    }

    @SmallTest
      public void testNothingToReplace() {

        List<ResolvedUrl> resolvedUrls = new ArrayList<ResolvedUrl>();
        resolvedUrls.add(MakeResolvedUrl("http://www.bing.com/","https://www.google.de"));

        testReplaceUrlsInText("Nothing to replace",resolvedUrls,"Nothing to replace");
    }

    @SmallTest
    public void testReplaceOneUrl() {

        List<ResolvedUrl> resolvedUrls = new ArrayList<ResolvedUrl>();
        resolvedUrls.add(MakeResolvedUrl("http://www.bing.com","https://www.google.de"));

        testReplaceUrlsInText("Here http://www.bing.com is something to replace!", resolvedUrls, "Here https://www.google.de is something to replace!");
    }

    @SmallTest
    public void testReplaceTwoUrls() {

        List<ResolvedUrl> resolvedUrls = new ArrayList<ResolvedUrl>();
        resolvedUrls.add(MakeResolvedUrl("http://www.bing.com","https://www.google.de"));

        testReplaceUrlsInText("Here http://www.bing.com is something http://www.bing.com to replace!", resolvedUrls, "Here https://www.google.de is something https://www.google.de to replace!");
    }

    @SmallTest
    public void testReplaceTwoDifferentUrls() {

        List<ResolvedUrl> resolvedUrls = new ArrayList<ResolvedUrl>();
        resolvedUrls.add(MakeResolvedUrl("http://www.bing.com","https://www.google.de"));
        resolvedUrls.add(MakeResolvedUrl("http://de.yahoo.com","http://web.de"));

        testReplaceUrlsInText("Here http://www.bing.com is something http://de.yahoo.com to replace!", resolvedUrls, "Here https://www.google.de is something http://web.de to replace!");
    }

    private ResolvedUrl MakeResolvedUrl(String rootUrl, String targetUrl) {
        return new ResolvedUrl(new Url(rootUrl), new Url(targetUrl));
    }

    private void testReplaceUrlsInText(String text, List<ResolvedUrl> resolvedUrls, String expectedReplacedText) {

        ResolvedUrlReplacer replacer = new ResolvedUrlReplacer();
        String replacedText = replacer.Replace(text, resolvedUrls);

        assertEquals(expectedReplacedText, replacedText);
    }
}
