package com.dkniep.ShortToLongUrl;

import android.test.suitebuilder.annotation.SmallTest;
import junit.framework.TestCase;
import java.io.IOException;

public class UrlResolverTest extends TestCase {

    public UrlResolverTest(){
        super();
    }

    @SmallTest
    public void testGoogleShortUrlWithOneRedirection() {

        testResolveUrl("http://goo.gl/ObmUey", "https://www.google.de/#newwindow=1&q=test&qscrl=1");
    }

    @SmallTest
    public void testBitlyShortUrlWithTwoRedirection() {

        testResolveUrl("http://bit.ly/JbzsHY", "https://www.google.de/#newwindow=1&q=test&qscrl=1");
    }

    @SmallTest
    public void testBitlyShortUrlWithOneRedirection() {

        testResolveUrl("http://bit.ly/1cqgUP1", "https://www.google.de/#newwindow=1&q=test&qscrl=1");
    }

    private void testResolveUrl(String rootLocation, String targetLocation) {

        Url rootUrl = new Url(rootLocation);
        Url targetUrl = new Url(targetLocation);

        UrlResolver resolver = new UrlResolver();
        try {
            ResolvedUrl resolved = resolver.Resolve(rootUrl);
            ResolvedUrl expectedResolved = new ResolvedUrl(rootUrl,targetUrl);
            AssertResolvedUrl(expectedResolved, resolved);

        } catch (IOException e) {
            fail(e.toString());
        }
    }

    private void AssertResolvedUrl(ResolvedUrl expectedResolved, ResolvedUrl resolved) {

        UrlAssertion.AssertUrlsAreEqual(expectedResolved.GetRootUrl(), resolved.GetRootUrl(), "RootUrl:");
        UrlAssertion.AssertUrlsAreEqual(expectedResolved.GetTargetUrl(), resolved.GetTargetUrl(), "TargetUrl:");
    }
}
