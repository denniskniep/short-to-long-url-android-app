package com.dkniep.ShortToLongUrl;

import junit.framework.Assert;

public class UrlAssertion {

    public static void AssertUrlsAreEqual(Url expectedUrl, Url url, String message)
    {
        if(!expectedUrl.GetUrl().equals(url.GetUrl()))
        {
            Assert.fail(message + " Url('" + url.GetUrl() + "') not equal to expectedUrl('" + expectedUrl.GetUrl() + "')");
        }
    }

    public static void AssertUrlsAreEqual(Url expectedUrl, Url url)
    {
        AssertUrlsAreEqual(expectedUrl,url,"");
    }
}
