package com.dkniep.ShortToLongUrl;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

import java.io.IOException;

public class UrlsInTextResolverTest extends TestCase {

    public UrlsInTextResolverTest(){
        super();
    }

    @SmallTest
    public void testTextWithRedirectedUrl() {

        testResolveText("This is some text http://goo.gl/ObmUey and here is some text too.", "This is some text https://www.google.de/#newwindow=1&q=test&qscrl=1 and here is some text too.");
    }

    @SmallTest
    public void testTextWithTwoRedirectedUrl() {

        testResolveText("This is some text http://goo.gl/ELoAUw and here is some text http://goo.gl/ObmUey too.", "This is some text https://www.google.de/#newwindow=1&q=abc&qscrl=1 and here is some text https://www.google.de/#newwindow=1&q=test&qscrl=1 too.");
    }

    private void testResolveText(String text, String expectedResolvedText) {

       UrlsInTextResolver resolver = new UrlsInTextResolver();
        try {
            String resolvedText = resolver.Resolve(text);
            assertEquals(expectedResolvedText, resolvedText);
        } catch (IOException e) {
            fail(e.toString());
        }
    }
}
