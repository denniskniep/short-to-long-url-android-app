package com.dkniep.ShortToLongUrl;

import android.test.suitebuilder.annotation.SmallTest;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class UrlExtractorTest extends TestCase {

    public UrlExtractorTest(){
        super();
    }

    @SmallTest
    public void testSingleUrlDomainOnly() {

        String text = "http://www.google.de";
        List<Url> urls = new ArrayList<Url>();
        urls.add(new Url("http://www.google.de"));
        testExtractUrls(text, urls);
    }

    @SmallTest
    public void testSingleHttpsUrlDomainOnly() {

        String text = "https://www.google.de";
        List<Url> urls = new ArrayList<Url>();
        urls.add(new Url("https://www.google.de"));
        testExtractUrls(text, urls);
    }

    @SmallTest
    public void testTextWithSingleUrlDomainOnly() {

        String text = "This is some text http://www.google.de and this is some text too.";
        List<Url> urls = new ArrayList<Url>();
        urls.add(new Url("http://www.google.de"));
        testExtractUrls(text, urls);
    }

    @SmallTest
    public void testTextWithSingleUrlDomainAndPathOnly() {

        String text = "This is some text https://www.google.de/#newwindow=1&q=test&qscrl=1 and this is some text too.";
        List<Url> urls = new ArrayList<Url>();
        urls.add(new Url("https://www.google.de/#newwindow=1&q=test&qscrl=1"));
        testExtractUrls(text, urls);
    }

    @SmallTest
    public void testSingleShortUrl() {

        String text = "http://goo.gl/ObmUey";
        List<Url> urls = new ArrayList<Url>();
        urls.add(new Url("http://goo.gl/ObmUey"));
        testExtractUrls(text, urls);
    }

    @SmallTest
    public void testTextWithSingleShortUrl() {

        String text = "This is some text http://goo.gl/ObmUey and this is some text too.";
        List<Url> urls = new ArrayList<Url>();
        urls.add(new Url("http://goo.gl/ObmUey"));
        testExtractUrls(text, urls);
    }

    @SmallTest
    public void testTextWithThreeShortUrls() {

        String text = "http://goo.gl/ObmUey This is some text http://www.google.de and this is some text too. https://www.google.de";
        List<Url> urls = new ArrayList<Url>();
        urls.add(new Url("http://goo.gl/ObmUey"));
        urls.add(new Url("http://www.google.de"));
        urls.add(new Url("https://www.google.de"));
        testExtractUrls(text, urls);
    }

    private void testExtractUrls(String text, List<Url> expectedUrls){
        UrlExtractor extractor = new UrlExtractor();
        List<Url> urls = extractor.ExtractUrls(text);

        AssertEqualUrls(expectedUrls, urls);
    }

    private void AssertEqualUrls(List<Url> expectedUrls, List<Url> urls) {

        if(expectedUrls.size() != urls.size())
        {
            fail("Size not equal");
        }

        for(int i = 0; i < expectedUrls.size(); i++)
        {
            Url expectedUrl = expectedUrls.get(i);
            Url url = urls.get(i);
            UrlAssertion.AssertUrlsAreEqual(expectedUrl, url);
        }
    }
}
